import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lighting_control/screens/home_screen.dart';
import 'package:lighting_control/screens/settings_screen.dart';
import 'package:lighting_control/screens/cleaning_screen.dart';
import 'package:lighting_control/services/settings_preferences.dart';
import 'package:provider/provider.dart';
import 'package:lighting_control/providers/page_name_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  ByteData data = await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
  SecurityContext.defaultContext.setTrustedCertificatesBytes(data.buffer.asUint8List());

  SettingsPreferences.init();

  runApp(MultiProvider(
    providers: [ChangeNotifierProvider(create: (_) => PageNameProvider())],
    child: MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/settings': (context) => const SettingsScreen(), //SettingsScreen(),
        '/cleaning': (context) => const CleaningScreen(),
      },
    ),
  ));
}
