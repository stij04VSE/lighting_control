// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'control_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ControlStore on _ControlStore, Store {
  Computed<StoreState>? _$stateComputed;

  @override
  StoreState get state => (_$stateComputed ??=
          Computed<StoreState>(() => super.state, name: '_ControlStore.state'))
      .value;

  final _$_controlStateFutureAtom =
      Atom(name: '_ControlStore._controlStateFuture');

  @override
  ObservableFuture<ControlState>? get _controlStateFuture {
    _$_controlStateFutureAtom.reportRead();
    return super._controlStateFuture;
  }

  @override
  set _controlStateFuture(ObservableFuture<ControlState>? value) {
    _$_controlStateFutureAtom.reportWrite(value, super._controlStateFuture, () {
      super._controlStateFuture = value;
    });
  }

  final _$controlStateAtom = Atom(name: '_ControlStore.controlState');

  @override
  ControlState? get controlState {
    _$controlStateAtom.reportRead();
    return super.controlState;
  }

  @override
  set controlState(ControlState? value) {
    _$controlStateAtom.reportWrite(value, super.controlState, () {
      super.controlState = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_ControlStore.errorMessage');

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$getControlStateAsyncAction =
      AsyncAction('_ControlStore.getControlState');

  @override
  Future<dynamic> getControlState(Control control) {
    return _$getControlStateAsyncAction
        .run(() => super.getControlState(control));
  }

  @override
  String toString() {
    return '''
controlState: ${controlState},
errorMessage: ${errorMessage},
state: ${state}
    ''';
  }
}
