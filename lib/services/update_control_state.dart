import 'package:http/http.dart' as http;
import 'package:lighting_control/models/read_value.dart';
import 'package:lighting_control/services/settings_preferences.dart';

class UpdateControlState {
  String? errorMessage;

  Future<void> updateControlState(ReadValue readValue, double newValue) async {
    try {
      errorMessage = null;

      // odeslání nového stavu
      await http.post(Uri.parse('${SettingsPreferences
          .getCommunicationProtocol()}://${SettingsPreferences
          .getApiEndpoint()}set=${readValue.GwID}'),
          headers: <String, String>{
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'c={"value": ${newValue.toInt()}}');

    } catch (e) {
      errorMessage = "Couldn't set new control state";
    }
  }

}
