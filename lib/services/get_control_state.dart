import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/services/settings_preferences.dart';

class GetControlState {
  Future<ControlState> getControlState(Control control) async {
    if (control.readValue != null) {
      var url = Uri.parse('${SettingsPreferences.getCommunicationProtocol()}://${SettingsPreferences.getApiEndpoint()}${control.readValue?.property}=${control.readValue?.GwID}');
      final response = await http.get(url);

      if (response.statusCode == 200) {
        print('Updating state of ${control.name} ...');
        return ControlState.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to load state');
      }
    }
    return ControlState(value: null);
  }

}
