import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/models/command.dart';
import 'package:lighting_control/services/settings_preferences.dart';

class SetControlState {
  String? errorMessage;

  Future<void> setControlState(List<Command> commands) async {
    for (Command command in commands) {
      try {
        errorMessage = null;

        // zjištění aktuálního stavu
        var url = Uri.parse('${SettingsPreferences.getCommunicationProtocol()}://${SettingsPreferences.getApiEndpoint()}get=${command.GwID}');
        final response = await http.get(url);

        double result = 0;
        if (response.statusCode == 200) {
          ControlState controlState = ControlState.fromJson(jsonDecode(response.body));
          if (controlState.value != null) {
            result = double.parse(controlState.value.toString());
          }
        } else {
          throw Exception('Unable to load the current state');
        }

        // nastavení nového stavu
        if (command.command == 'increaseValue') {
          result += command.value;
        } else if (command.command == 'decreaseValue') {
          result -= command.value;
        } else if (command.command == 'setValue') {
          result = double.parse(command.value.toString());
        }

        if (result > 100) {
          result = 100;
        } else if (result < 0) {
          result = 0;
        }

        // odeslání nového stavu
        await http.post(Uri.parse('${SettingsPreferences.getCommunicationProtocol()}://${SettingsPreferences.getApiEndpoint()}${command.property}=${command.GwID}'),
            headers: <String, String>{
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            body:
            'c={"value": ${result.toInt()}}'
        );

      } catch (e) {
        errorMessage = "Couldn't set new control state";
      }
    }
  }

}
