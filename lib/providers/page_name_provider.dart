import 'package:flutter/material.dart';

class PageNameProvider with ChangeNotifier {
  String _pageName = 'Lighting control panel';

  String get pageName => _pageName;

  void setPageName(String newPageName) {
    _pageName = newPageName;
    notifyListeners();
  }

}