import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/services/control_store.dart';
import 'package:lighting_control/services/get_control_state.dart';
import 'package:lighting_control/theme/text_styles.dart';
import 'package:lighting_control/widgets/control_label.dart';
import 'package:lighting_control/widgets/control_value.dart';
import 'package:lighting_control/widgets/control_light.dart';
import 'package:lighting_control/widgets/control_button.dart';
import 'package:lighting_control/widgets/control_slider.dart';

class CustomControl extends StatefulWidget {
  final Control control;

  const CustomControl({Key? key, required this.control}) : super(key: key);

  @override
  _CustomControlState createState() => _CustomControlState();
}

class _CustomControlState extends State<CustomControl> {
  static TextStyles textStyles = TextStyles();
  ControlStore? _controlStore;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _controlStore = ControlStore(GetControlState());
    _controlStore?.getControlState(widget.control);
    _timer = Timer.periodic(const Duration(seconds: 1),
        (_) => _controlStore?.getControlState(widget.control));
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.control.type == 'label') {
      return ControlLabel(control: widget.control);
    } else if (widget.control.type == 'button' && widget.control.readValue == null) {
      return ControlButton(control: widget.control);
    } else {
      return Observer(builder: (context) {
        switch (widget.control.type) {
          case 'value': {
            return ControlValue(controlState: _controlStore?.controlState, control: widget.control);
          }
          case 'light': {
            return ControlLight(controlState: _controlStore?.controlState, control: widget.control);
          }
          case 'button': {
            return ControlButton(controlState: _controlStore?.controlState, control: widget.control);
          }
          case 'slider': {
            return ControlSlider(controlState: _controlStore?.controlState, control: widget.control);
          }
          default: {
            return Center(child: Text('${_controlStore?.controlState?.value}', style: textStyles.button));
          }
        }
      });
    }
  }

}
