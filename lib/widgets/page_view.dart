import 'package:flutter/material.dart';
import 'package:flutter_layout_grid/flutter_layout_grid.dart';
import 'package:page_view_indicators/page_view_indicators.dart';
import 'package:lighting_control/models/device.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/widgets/control.dart';
import 'package:provider/provider.dart';
import 'package:lighting_control/providers/page_name_provider.dart';

class CustomPageView extends StatefulWidget {
  final Device device;

  const CustomPageView({Key? key, required this.device}) : super(key: key);

  @override
  _CustomPageViewState createState() => _CustomPageViewState();
}

class _CustomPageViewState extends State<CustomPageView> {
  static const colors = AppColors();
  static const padding = 5.0; // velikost mezery mezi ovládacími prvky

  final _pageController = PageController(initialPage: 0);
  final _currentPageNotifier = ValueNotifier<int>(0);
  List<Widget> _pageList = <Widget>[];

  @override
  Widget build(BuildContext context) {
    _pageList.clear();
    _pageList = createPageList();

    WidgetsBinding.instance?.addPostFrameCallback((_) => context.read<PageNameProvider>().setPageName(widget.device.pages.first.name));

    return Stack(
      children: <Widget>[
        _buildPageView(),
        _buildCircleIndicator(),
      ]
    );
  }

  List<Widget> createPageList() {
    for (var page in widget.device.pages) {
      List<Widget> controlList = <Widget>[];

      for (var control in page.controls) {
        controlList.add(GridPlacement(
            columnStart: control.x,
            rowStart: control.y,
            columnSpan: control.width,
            rowSpan: control.height,
            child: Padding(
              padding: const EdgeInsets.all(padding),
              child: CustomControl(control: control),
            )
        ));
      }

      _pageList.add(Container(
        color: colors.background,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(padding, padding, padding, padding * 2),
          child: LayoutGrid(
              columnSizes: List.filled(page.width, 1.fr),
              rowSizes: List.filled(page.height, 1.fr),
              children: controlList),
        ),
      ));
    }

    return _pageList;
  }

  _buildPageView() {
    return PageView.builder(
        pageSnapping: true,
        itemCount: _pageList.length,
        controller: _pageController,
        itemBuilder: (BuildContext context, int index) {
          return _pageList[index];
        },
        onPageChanged: (int index) {
          _currentPageNotifier.value = index;
          context.read<PageNameProvider>().setPageName(widget.device.pages[index].name);
        });
  }

  _buildCircleIndicator() {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: CirclePageIndicator(
        itemCount: _pageList.length,
        currentPageNotifier: _currentPageNotifier,
      ),
    );
  }

}
