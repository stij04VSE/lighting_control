import 'package:flutter/material.dart';
import 'package:lighting_control/models/control.dart';
import 'package:lighting_control/models/control_state.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';
import 'package:lighting_control/services/update_control_state.dart';

class ControlSlider extends StatefulWidget {
  final Control control;
  final ControlState? controlState;

  const ControlSlider(
      {Key? key, required this.control, required this.controlState})
      : super(key: key);

  @override
  State<ControlSlider> createState() => _ControlSliderState();
}

class _ControlSliderState extends State<ControlSlider> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final UpdateControlState _updateControlState = UpdateControlState();
  double _value = 0;
  double _updatedValue = 0;
  bool _isMoving = false;

  @override
  Widget build(BuildContext context) {
    _updatedValue = widget.controlState?.value != null ? double.parse(widget.controlState!.value.toString()) : 0;

    return Container(
        decoration: BoxDecoration(
            color: colors.controlSlider,
            borderRadius: const BorderRadius.all(Radius.circular(10.0))),
        child: SliderTheme(
            data: SliderTheme.of(context).copyWith(
              trackHeight: 10.0,
              trackShape: const RoundedRectSliderTrackShape(),
              activeTrackColor: colors.controlSliderActive,
              inactiveTrackColor: colors.controlSliderInactive,
              thumbShape: const RoundSliderThumbShape(
                enabledThumbRadius: 14.0,
                pressedElevation: 8.0,
              ),
              thumbColor: colors.controlSliderThumb,
              overlayColor: colors.controlSliderInactive.withOpacity(0.2),
              overlayShape: const RoundSliderOverlayShape(overlayRadius: 32.0),
              valueIndicatorShape: const PaddleSliderValueIndicatorShape(),
              valueIndicatorColor: colors.controlSliderActive,
              valueIndicatorTextStyle: textStyles.controlSlider
            ),
            child: RotatedBox(
              quarterTurns: widget.control.height > widget.control.width ? -1 : 0,
              child: Slider.adaptive(
                divisions: 100,
                label: '${_value.round()}',
                value: _isMoving ? _value : _value = _updatedValue,
                min: 0.0,
                max: 100.0,
                onChangeStart: (_) {
                  _isMoving = true;
                },
                onChanged: (newValue) => setState(() {
                  _value = newValue;
                }),
                onChangeEnd: (newValue) {
                  if (widget.control.readValue != null) {
                    _updateControlState.updateControlState(widget.control.readValue!, newValue);
                  }
                  _isMoving = false;
                }
              ),
            )
        )
    );
  }

}
