import 'package:flutter/material.dart';
import 'package:lighting_control/theme/colors.dart';

@immutable
class TextStyles {
  static const colors = AppColors();

  final title = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final dateTime = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final menuItem = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 18.0, letterSpacing: 0.5);
  final button = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final textField = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, letterSpacing: 0.5);

  final label = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);

  final passcode = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 1.0);

  // controls text style
  final controlLabel = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final controlValue = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final controlLight = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final controlButton = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final controlSlider = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);

  // cleaning screen text styles
  final cleaningScreenLabel = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, letterSpacing: 0.5);
  final cleaningScreenValue = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, letterSpacing: 0.5);
  final cleaningScreenValueHighlight = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);

  // settings screen text styles
  final settingsSectionTile = TextStyle(fontFamily: 'Raleway', color: colors.textHighlighted, fontSize: 15.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final settingsTileTitle = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 18.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final settingsTileSubtitle = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 15.0, letterSpacing: 0.5);
  final settingsDialogTitle = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 20.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final settingsTextField = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 18.0, letterSpacing: 0.5);
  final settingsSubmit = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 18.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);

  final deviceName = TextStyle(fontFamily: 'Raleway', color: colors.textHighlighted, fontSize: 22.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final version = TextStyle(fontFamily: 'Raleway', color: colors.textHighlighted, fontSize: 15.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
  final noData = TextStyle(fontFamily: 'Raleway', color: colors.text, fontSize: 40.0, fontWeight: FontWeight.bold, letterSpacing: 0.5);
}