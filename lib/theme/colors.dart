import 'package:flutter/material.dart';

@immutable
class AppColors {
  final background = const Color(0xff121212);
  final text = const Color(0xffffffff);
  final textHighlighted = const Color(0xff80c342);
  final buttonOn = const Color(0xff80c342);
  final buttonOff = const Color(0xff2f2f2f);
  final panel = const Color(0xff2f2f2f);
  final focus = const Color(0xff03a9f4);

  // passcode screen colors
  final passcodeScreen = const Color(0xff80c342);

  // settings screen colors
  final settingsTile = const Color(0xff2f2f2f);

  // controls colors
  // label
  final controlLabel = const Color(0xff121212);
  // value
  final controlValue = const Color(0xff9e9e9e);
  // light
  final controlLightOn = const Color(0xff80c342);
  final controlLightOff = const Color(0xff2f2f2f);
  // button
  final controlButtonOn = const Color(0xff80c342);
  final controlButtonOff = const Color(0xff2f2f2f);
  final controlButtonIcon = const Color(0xffffffff);
  //slider
  final controlSlider = const Color(0xff2f2f2f);
  final controlSliderActive = const Color(0xff80c342);
  final controlSliderInactive = const Color(0xff9e9e9e);
  final controlSliderThumb = const Color(0xffffffff);

  const AppColors();
}