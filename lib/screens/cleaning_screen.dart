import 'dart:async';
import 'package:flutter/material.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';

class CleaningScreen extends StatefulWidget {
  const CleaningScreen({Key? key}) : super(key: key);

  @override
  State<CleaningScreen> createState() => _CleaningScreenState();
}

class _CleaningScreenState extends State<CleaningScreen> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  late Timer _timer;
  late int _tick;

  @override
  void initState() {
    super.initState();
    _tick = 30;
    _timer = Timer.periodic(const Duration(seconds: 1), (_) {
      setState(() {
        _tick--;
      });
    });

    Timer(const Duration(seconds: 30), () => {Navigator.pop(context), Navigator.pop(context)});
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: true,
      child: Container(
        color: colors.background,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              DefaultTextStyle(
                style: textStyles.cleaningScreenLabel,
                child: const Text('This is a cleaning screen, the touch control is locked for 30 seconds.'),
              ),
              const SizedBox(height: 50),
              DefaultTextStyle(
                style: textStyles.cleaningScreenValueHighlight,
                child: Text('$_tick seconds '),
              ),
              const SizedBox(height: 10),
              DefaultTextStyle(
                style: textStyles.cleaningScreenValue,
                child: const Text('remaining to clean the screen')
              ),
              const SizedBox(height: 50),
              DefaultTextStyle(
                style: textStyles.cleaningScreenLabel,
                child: const Text('You will then be redirected to the home screen.')
              )
            ],
          ),
        ),
      ),
    );
  }

}
