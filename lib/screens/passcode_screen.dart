import 'dart:async';
import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:lighting_control/theme/colors.dart';
import 'package:lighting_control/theme/text_styles.dart';

const storedPasscode = '123456';

class CustomPasscodeScreen extends StatefulWidget {
  const CustomPasscodeScreen({Key? key}) : super(key: key);

  @override
  State<CustomPasscodeScreen> createState() => _CustomPasscodeScreenState();
}

class _CustomPasscodeScreenState extends State<CustomPasscodeScreen> {
  static const colors = AppColors();
  static TextStyles textStyles = TextStyles();
  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PasscodeScreen(
      title: Text(
          'Enter passcode',
          textAlign: TextAlign.center,
          style: textStyles.passcode
      ),
      backgroundColor: colors.background.withOpacity(0.9),
      circleUIConfig: CircleUIConfig(
          borderColor: colors.passcodeScreen,
          fillColor: colors.passcodeScreen,
          circleSize: 30),
      keyboardUIConfig: KeyboardUIConfig(
          digitBorderWidth: 2,
          primaryColor: colors.passcodeScreen),
      cancelButton: Text(
        'Cancel',
        style: textStyles.passcode,
        semanticsLabel: 'Cancel',
      ),
      deleteButton: Text(
        'Delete',
        style: textStyles.passcode,
        semanticsLabel: 'Delete',
      ),
      shouldTriggerVerification: _verificationNotifier.stream,
      passwordEnteredCallback: _onPasscodeEntered,
      cancelCallback: _onPasscodeCancelled,
      isValidCallback: () => Navigator.popAndPushNamed(context, '/settings'),
      passwordDigits: 6,
    );
  }

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = storedPasscode == enteredPasscode;
    _verificationNotifier.add(isValid);
    if (isValid) {
      setState(() {});
    }
  }

  _onPasscodeCancelled() {
    //Navigator.maybePop(context);
    Navigator.pop(context);
    Navigator.pop(context);
  }

}
