import 'package:lighting_control/models/page.dart';

class Device {
  final String deviceName;
  final String token;
  final List<Page> pages;

  const Device({
    required this.deviceName,
    required this.token,
    required this.pages
  });

  factory Device.fromJson(Map<String, dynamic> json) {
    var pagesObjJson = json['pages'] as List;
    List<Page> _pages = pagesObjJson.map((pageJson) => Page.fromJson(pageJson)).toList();
    return Device(
        deviceName: json['deviceName'] as String,
        token: json['token'] as String,
        pages: _pages
    );
  }

}