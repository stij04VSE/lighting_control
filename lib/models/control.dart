import 'package:lighting_control/models/command.dart';
import 'package:lighting_control/models/read_value.dart';

class Control {
  final String name;
  final String type;
  final int x;
  final int y;
  final int width;
  final int height;
  final ReadValue? readValue;
  dynamic feedbackValue;
  List<Command>? commands;

  Control({
    required this.name,
    required this.type,
    required this.x,
    required this.y,
    required this.width,
    required this.height,
    required this.readValue,
    required this.feedbackValue,
    required this.commands
  });

  factory Control.fromJson(Map<String, dynamic> json) {
    var commandsObjJson = json['commands'] as List?;
    List<Command>? _commands = commandsObjJson?.map((commandJson) => Command.fromJson(commandJson)).toList();

    return Control(
        name: json['name'] as String,
        type: json['type'] as String,
        x: json['x'] as int,
        y: json['y'] as int,
        width: json['width'] as int,
        height: json['height'] as int,
        readValue: json['readValue'] == null ? null : ReadValue.fromJson(json['readValue']),
        feedbackValue: json['feedbackValue'],
        commands: _commands
    );
  }

}