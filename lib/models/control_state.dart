class ControlState {
  dynamic value;

  ControlState({
    required this.value,
  });

  factory ControlState.fromJson(Map<String, dynamic> json) {
    return ControlState(value: json['value']);
  }

}
